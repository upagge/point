import math
import random
import matplotlib.pyplot as plt

class cloud_point(object):

    def __init__(self,n,r):
        self.mas = []
        for i in range(n):
            self.mas.append(self.point(r))

    class point(object):

        def __init__(self,r):
            self.r2 = random.uniform(0, r)
            self.t = random.randint(0, 360)
            self.x = self.r2 * math.cos(self.t)
            self.y = self.r2 * math.sin(self.t)
            self.dy = random.uniform(-1, 1)
            self.dx = random.uniform(-1, 1)

        def get_x(self):
            return self.x
        def get_y(self):
            return self.y
        def get_r(self):
            return self.r2
        def get_dy(self):
            return self.dy
        def get_dx(self):
            return self.dx

    def get_cloud_x(self):
        mas = []
        for elem in self.mas:
            mas.append(elem.get_x())
        return mas

    def get_cloud_y(self):
        mas = []
        for elem in self.mas:
            mas.append(elem.get_y())
        return mas

    def get_cloud_dx(self):
        mas = []
        for elem in self.mas:
            mas.append(elem.get_dx())
        return mas

    def get_cloud_dy(self):
        mas = []
        for elem in self.mas:
            mas.append(elem.get_dy())
        return mas

    def import_file(self,name):
        f = open(name, 'w')
        for elem in self.mas:
            f.write(str(elem.get_x()) + ' ' + str(elem.get_y()) + ' ' + str(elem.get_dx()) + ' ' + str(
                elem.get_dy()) + '\n')
        return f.close()

    def diafragma(self,r):
        plt.figure()
        ax = plt.gca()
        ax.quiver(self.get_cloud_x(), self.get_cloud_y(), self.get_cloud_dx(), self.get_cloud_dy(), angles='xy',
                  scale_units='xy', scale=1)
        ax.set_xlim([-r, r])
        ax.set_ylim([-r, r])
        plt.draw()
        plt.show()

    def pic_point(self,name):
        fig, ax = plt.subplots()
        ax.scatter(self.get_cloud_x(), self.get_cloud_y(), s=0.5)
        fig.savefig(name)



r = input('Введите радиус окружности: ')
n = input('Введите количество точек: ')
r = float(r)
n = int(n)

cloud = cloud_point(n,r)
cloud.import_file('points.txt')
cloud.pic_point('point.png')
cloud.diafragma(r)








